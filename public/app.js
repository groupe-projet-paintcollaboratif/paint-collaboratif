var socket;

function setup() {
    createCanvas(1000, 800);
    background(51);

    socket = io.connect('http://127.0.1.1:3030');
    socket.on('mouse', newDrawing);

}
function newDrawing(data) {
    noStroke();
    fill(100, 300, 200);
    ellipse(data.x, data.y, 36, 36);
}
function mouseDragged(){
    console.log('Envoyer: ' + mouseX + ',' + mouseY);

    var data = {
        x: mouseX,
        y: mouseY
    }
    socket.emit('mouse', data);
    noStroke();
    fill(200, 0, 100);
    ellipse(data.x, data.y, 36, 36);
}

function draw() {
    }