let express = require('express');
let app = express();
let http = require('http').createServer(app);
let io = require('socket.io')(http);

app.use(express.static('public'));

app.get('/', function(require, response) {
    response.sendFile('index.html');
});
io.sockets.on('connection', newConnection);

function newConnection(socket) {
    console.log('nouvelle connection' + ' ' + socket.id);

    socket.on('mouse', mouseMsg);

    function mouseMsg(data) {
        socket.broadcast.emit('mouse', data);
        console.log(data);
    }
}
http.listen(3030, function() {
    console.log('listening on *:3030')
});